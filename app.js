const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const logger = require('morgan');
const app = express();
const passport = require('passport');
const port = process.env.PORT || 4000;
require('dotenv').config();


// for using cors
app.use(cors());
// /body-parser middleware
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.use(logger('combined'));

// session
app.use(cookieParser());
app.use(session({
    secret: process.env.secret,
    resave: true,
    saveUninitialized: false,
    cookie : {
        expires: false,
    }
}));

//Passport
app.use(passport.initialize());
app.use(passport.session());
// calling passport strategy function
require('./config/passport')(passport);

app.use(express.static(path.join(__dirname,'/dist/mean-ang')));

app.get('/',(req,res)=>{
    // res.send('welcome ');
    res.sendFile(path.join(__dirname,'dist/mean-ang/index.html'));
});

mongoose.connect(process.env.mongoURI,{ useNewUrlParser: true })
    .then(()=>{
        console.log('connected mongo');
    })
    .catch((err)=>{
        console.log(err);
    });

// users routes
const api = require('./server/book');
app.use('/api',api);
const users = require('./server/users');
app.use('/users',users);

app.get('/welcome',(req,res)=>{
    res.send('welcome work');
});

// for making payment
const paypal = require('./server/paypal');
app.use('/paypal',paypal);

// for all other routes
app.get('**',(req,res)=>{
    res.sendFile(path.join(__dirname,'./dist/mean-ang/index.html'));
});

app.listen(port,()=>{
    console.log(`listening on ${port}`);
});