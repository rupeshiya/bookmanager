const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosastic = require('mongoosastic');

var BookSchema  = new Schema({
    isbn:{
        type: String,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    author:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    review:{
        type: []
    },
    published_year:{
        type: String
    },
    publisher:{
        type: String
    },
    updated_date:{
        type: Date,
        default: Date.now()
    },
    data:{
        image:{ data: Buffer , type: String }
    },
    reviews:{
        type: mongoose.SchemaTypes.ObjectId ,ref:'Reviews'
    },
    downVote:{
        type: Number,
        default: 0
    },
    upVote:{
        type: Number,
        default: 0
    }
});

// // adding elasticsearch plugins
// BookSchema.plugin(mongoosastic,{
//     hosts: [
//         'localhost: 9200'
//     ]
// });


module.exports = mongoose.model('Book',BookSchema);
