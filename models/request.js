const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookRequestSchema = new Schema({
    requesterEmail:{
        type: String,
        required: true
    },
    bookName:{
        type: String,
        required: true
    },
    authorName:{
        type: String,
        required: true
    }
});

module.exports = mongoose.model('BookRequest',BookRequestSchema);