const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    review: {
        type: [ { }]
    },
    book: {
        type: mongoose.SchemaTypes.ObjectId, ref: 'Book'
    }
});

const review = mongoose.model('Reviews',reviewSchema);
module.exports = review;