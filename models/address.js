const mongoose = require('mongoose');
const express = require('express');
const Schema = mongoose.Schema;

const addressSchema = new Schema({
    city:{
        type: String,
    },
    state:{
        type: String,
    },
    mobNumber:{
        type: Number    
    },
    flatNumber:{
        type: String
    },
    nearby:{
        type: String
    },
    country:{
        type: String
    },
    pinCode:{
        type: String
    },
    user: {
        type: mongoose.SchemaTypes.ObjectId, ref: 'Users'
    }
});
// created for using this in our function 
mongoose.model('AdressSchema',addressSchema);

module.exports = mongoose.model('AdressSchema',addressSchema);
