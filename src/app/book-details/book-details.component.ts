import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';
import { ActivatedRoute, Router  } from '@angular/router';
import { SharedServicesService } from '../services/shared-services.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
  book: any = [];
  id: any;
  title: any;

  constructor(
    private data: DataServiceService,
    private route: ActivatedRoute,
    private router: Router,
    private sharedService: SharedServicesService,
    private falshMessage: FlashMessagesService
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });

    // to get book data by id
    this.getBookById(this.id);
  }

  // function to getBookById
  getBookById(id: any) {
    this.data.getBook(id)
      .subscribe(book => {
        this.book = book;
        this.title = book.title;
      });
  }

  // on add to cart saving to shared service
  onAddToCart() {
    this.sharedService.saveToCart(this.book, this.id);
    this.falshMessage.show('Successfully added to the cart', {cssClass: 'alert-success', timeout: 3000});
  }
}
