export class Book {
  _id?: string;
  isbn?: string;
  author?: string;
  title?: string;
  published_year?: string;
  publisher?: string;
  description?: string;
  review?: string;
}
