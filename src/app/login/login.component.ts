import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SharedServicesService } from '../services/shared-services.service';
// import { sendgridMail } from '../../../server/mail';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  role: any;
  admin: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private sharedService: SharedServicesService
    ) { }

  ngOnInit() {
    // checking for admin
    this.adminFound();
  }
  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password,
      role: this.role
    };
    this.authService.getAuthenticated(user).subscribe(data => {
      if (data.success) {
        this.flashMessage.show('Successfully logged In!!', {cssClass: 'alert-success', timeout: 3000});
        this.authService.storeUserData(data.token, data.user);
        // this.sendMail();
        console.log('user token-', data.token);
        console.log('user info-', data.user);
        this.router.navigate(['/books']);
      } else {
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 5000});
        this.router.navigate(['/login']);
      }
    });
  }
  adminFound() {
    this.authService.getAdmin().subscribe(data => {
      if (data.success) {
        if (data.admin.length > 0) {
          this.admin = true;
          // window.location.reload();
          return this.admin;
        }
      } else {
          this.admin = false;
          window.location.reload();
          return this.admin;
        }

    });
  }
  onForgotClick() {
    this.router.navigate(['/email']);
  }
}
