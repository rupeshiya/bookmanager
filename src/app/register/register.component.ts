import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { SharedServicesService } from '../services/shared-services.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: string;
  username: string;
  password: string;
  email: string;
  role: any;
  admin: boolean;
  constructor(
    private validate: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router,
    private sharedServices: SharedServicesService
    ) { }

  ngOnInit() {
    // checking for the admin
    this.adminFound();
  }

  // event for register button click
  onRegisterSubmit() {
    const user = {
      // since we have done double binding so we can access this property from here
      name: this.name,
      username: this.username,
      password: this.password,
      email: this.email,
      role: this.role
    };
    if (!this.validate.validateRegister(user)) {
      this.flashMessage.show('Please fill all the info !!', {cssClass: 'alert-danger', timeout: 2000});
      console.log('Please fill all the info !!');
    }
    if (!this.validate.validateEmail(user.email)) {
      this.flashMessage.show('Please enter a valid email !!', {cssClass: 'alert-danger', timeout: 2000});
      console.log('Please enter the valid email !!');
    }
    this.authService.getRegister(user)
      .subscribe(res => {
        if (res.success) {
          console.log('response from register component using auth service ', res);
          this.flashMessage.show(`${res.msg}`, {cssClass: 'alert-success', timeout: 5000});
          window.location.reload();
          this.router.navigate(['/login']);
        } else {
          this.flashMessage.show(`${res.msg}`, {cssClass: 'alert-info', timeout: 5000});
          // window.location.reload();
          this.router.navigate(['/register']);
        }
      });
  }

  // checking for the admin presence
  adminFound() {
    this.authService.getAdmin().subscribe(data => {
      if (data.success) {
        if (data.admin.length > 0) {
          this.admin = true;
          return this.admin;
        }
      } else {
          this.admin = false;
          return this.admin;
        }
    });
  }
}
