import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookComponent } from './book/book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookCreateComponent } from './book-create/book-create.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataServiceService } from './data-service.service';
import { HttpClientModule  } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthService } from './services/auth.service';
import { ValidateService } from './services/validate.service';
import { AuthGuard } from './guards/auth.guards';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AboutComponent } from './about/about.component';
import { SearchDataComponent } from './search-data/search-data.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { AdminComponent } from './admin/admin.component';
import { ReviewComponent } from './review/review.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CartComponent } from './cart/cart.component';
import { AddAdressComponent } from './add-adress/add-adress.component';
import { EmailComponent } from './email/email.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { BookRequestComponent } from './book-request/book-request.component';


@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    BookDetailsComponent,
    BookCreateComponent,
    BookEditComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    AboutComponent,
    SearchDataComponent,
    AdminComponent,
    ReviewComponent,
    HomeComponent,
    DashboardComponent,
    CartComponent,
    AddAdressComponent,
    EmailComponent,
    UpdatePasswordComponent,
    BookRequestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    MaterialModule,
    // ReactiveFormsModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    FlashMessagesModule.forRoot()
  ],
  providers: [DataServiceService, HttpClientModule, AuthGuard, AuthService, ValidateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
