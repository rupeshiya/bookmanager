import { Component, OnInit } from '@angular/core';
// import Services to use
import { DataServiceService } from '../data-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {
book: any = [];
id: any;
bookById: any;
isbn: any;
author: any;
title: any;
publisher: any;

  constructor(
    private data: DataServiceService,
    private route: ActivatedRoute ,
    private router: Router,
    private dataService: DataServiceService,
    private authService: AuthService) { }

  ngOnInit() {
    // this.data.updateBook(this.route.snapshot.params['id'])
    //   .subscribe((book)=>{
    //     console.log('edit component',book);
    //     this.book = book;
    //     console.log('edit book',this.book);
    //   })
    this.route.params.subscribe(params => {
      this.id = params.id;
      console.log('params', this.id);
    });
    this.getBookById(this.id);
  }
  onSubmit(form: NgForm) {
    console.log('form', form.value);
    this.book =  form.value;
    const obj = {
      isbn: this.book.isbn,
      title: this.book.title,
      author: this.book.author,
      publisher: this.book.publisher
    };
    this.data.updateBook(this.id, obj)
    .subscribe(
      res =>
      this.router.navigate([`/books`])
      );
  }
  onSubmitButton() {
    console.log('Submit button clicked');
  }

  getBookById(id) {
    this.dataService.getBook(id).subscribe(data => {
      console.log('data from getBookById from book-edit component -', data);
      this.bookById = data;
      this.isbn = data.isbn;
      this.author = data.author;
      this.title = data.title;
      this.publisher = data.publisher;
    });
  }
}
