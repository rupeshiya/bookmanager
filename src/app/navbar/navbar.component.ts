import { Component, OnInit , Output, EventEmitter } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DataServiceService } from '../data-service.service';
import { SharedServicesService } from '../services/shared-services.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
searchText: any;
books: any;
admin: boolean;
searchDone = false;
  constructor(
    public authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private dataService: DataServiceService,
    private sharedService: SharedServicesService
    ) { }

  ngOnInit() {
    this.adminFound();
  }
  onLogoutClick() {
    this.authService.logout();
    this.flashMessage.show('You are successfully logged out !!', {cssClass: 'alert-success', timeout: 3000});
    this.router.navigate(['/login']);
  }

  // function to search for the book
  onSearchClick() {
    console.log('searchText-', this.searchText);
    // call the function to save the data in services
    this.sharedService.saveData(this.searchText);
    this.router.navigate(['/search-data']);
  }

  // function to check serach done
  // searchFound(){
  //   this.sharedService.getData().subscribe(data =>{
  //     const books: any = data;
  //     if(books.length > 0){
  //       this.searchDone = true;
  //       this.onSearchClick();
  //       return true;
  //       console.log('search done function called ');
  //     } else {
  //         this.searchDone = false;
  //         return false;
  //     }
  //   })
  // }
  onLogin() {
    console.log('send mail..');
    // this.sharedService.sendgridMail();
  }


  adminFound() {
    this.authService.getAdmin().subscribe(data => {
      if (data.success) {
          if (data.admin.length > 0) {
            console.log('mssg from navbar component admin-', data.admin);
            this.admin = true;
            this.flashMessage.show('Admin found !!', {cssClass: 'alert-success', timeout: 3000});
            return this.admin;
          }
         } else {
            this.admin = false;
            this.flashMessage.show('Admin not found !!', {cssClass: 'alert-success', timeout: 3000});
            window.location.reload();
            return this.admin;
          }
    });
  }

  // on click of cart
  onClickCart() {
    this.router.navigate(['/cart']);
  }
}
