import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
email: any;
  constructor(
    private router: Router,
    private authService: AuthService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onResetClick() {
    this.authService.forgotPassword(this.email).subscribe((res) => {
      if (res.success) {
        console.log('successfully sent the request !!');
        this.flashMessage.show('Please check your email for reset link !!', {cssClass: 'alert-success', timeout: 3000});
      } else {
        console.log('something went wrong !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }
}
