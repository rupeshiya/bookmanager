import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book/book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookCreateComponent } from './book-create/book-create.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { AuthGuard } from './guards/auth.guards';
import { AboutComponent } from './about/about.component';
import { SearchDataComponent } from './search-data/search-data.component';
import { AdminComponent } from './admin/admin.component';
import { ReviewComponent } from './review/review.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CartComponent } from './cart/cart.component';
import { AddAdressComponent } from './add-adress/add-adress.component';
import { EmailComponent } from './email/email.component';
import { BookRequestComponent } from './book-request/book-request.component';
// import { BookDeleteComponent } from './book-delete/'

const routes: Routes = [
  {
    path: 'books',
    component: BookComponent,
    data: { title: 'Book List' },
    // canActivate: [AuthGuard]
  },
  {
    path: 'book-details/:id',
    component: BookDetailsComponent,
    data: { title: 'Book Details' },
    // canActivate: [AuthGuard]
  },
  {
    path: 'book-create',
    component: BookCreateComponent,
    data: { title: 'Create Book' },
    canActivate: [AuthGuard]
  },
  {
    path: 'book-edit/:id',
    component: BookEditComponent,
    data: { title: 'Edit Book' },
    canActivate: [AuthGuard]
  },
  // {
  //   path: 'book-delete/:id',
  //   component: BookDeleteComponent
  // },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'search-data',
    component: SearchDataComponent,
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'review/:id',
    component: ReviewComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'addAddress/:id',
    component: AddAdressComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'book-request',
    component: BookRequestComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'email',
    component: EmailComponent
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
