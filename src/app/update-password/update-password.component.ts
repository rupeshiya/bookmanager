import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {
username: any;
password: any;
confirmPassword: any;

  constructor(
    private router: Router,
    private authService: AuthService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onSubmitClick() {
    const username = {
      username: this.username
    };
    this.authService.updatePassword(username).subscribe((res) => {
      if (res.success) {
        console.log('Update the password !!');
        this.flashMessage.show('Successfully updated the password !!', {cssClass: 'alert-success', timeout: 3000});
      } else {
        console.log('error in updating the password !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }
}
