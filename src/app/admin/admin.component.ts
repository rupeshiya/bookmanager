import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { users } from '../interface';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
admins: any;
  constructor(
    private authService: AuthService,
    private flasMessages: FlashMessagesService,
    private router: Router
    ) { }

  ngOnInit() {
    // for getting the admin data
    this.adminData();
  }

  adminData() {
    this.authService.getAdmin().subscribe(data => {
      if (data.success) {
        this.admins = data.admin;
        if (data.admin.length > 0) {
            this.flasMessages.show('Admin found !!', {cssClass: 'alert-success', timeout: 3000});
        }
      } else {
        this.flasMessages.show('Admin not found !!', {cssClass: 'alert-success', timeout: 3000});
        console.log('No admin found -msg from admin component');
      }
    });
  }

  removeAdmin(id) {
    this.authService.removeAdmin(id).subscribe(data => {
      if (data.success) {
        console.log('admin removed');
        this.flasMessages.show('Admin removed', {cssClass: 'alert-success', timeout: 3000});
        // added this to update the admin boolean value bug
        window.location.reload();
      } else {
        console.log('unable to remove admin');
        this.flasMessages.show('Something went wrong !!', {cssClass: 'alert-denger', timeout: 3000});
      }
    });
  }

}
