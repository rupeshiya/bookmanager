import { Component, OnInit } from '@angular/core';
import { SharedServicesService } from '../services/shared-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../book';
import { DataServiceService } from '../data-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
id: any;
// book: Book;
reviews: any;
review: string;
selectedFile: File;
abusiveWords: any =  /crap|ugly|brat|fool|fuck|fucking|f\*cking|f\*ck|bitch|b\*tch|shit|sh\*t|fool|dumb|couch potato|arse|arsehole|asshole|\*ssh\*l\*|\*\*\*\*|c\*ck|\*\*\*\*sucker|c\*cks\*ck\*r|\*\*\*\*|c\*nt|dickhead|d\*c\*h\*a\*|\*\*\*\*|f\*c\*|\*\*\*\*wit|f\*ckw\*t|fuk|f\*k|fuking|f\*k\*ng|mother\*\*\*\*er|m\*th\*rf\*ck\*r|\*\*\*\*\*\*|n\*gg\*r|pussy|p\*ssy|\*\*\*\*|sh\*t|wanker|w\*nk\*r|wankers|w\*nk\*rs|whore|wh\*r\*|slag| sl\*g|\*\*\*\*\*|b\*tch|f u c k|f\*c\*|b.i.t.c.h|b\*tch|d-i-c-k|d\*\*\*/gi;

  constructor(
    private sharedService: SharedServicesService,
    private route: ActivatedRoute,
    private dataService: DataServiceService,
    private flashMessage: FlashMessagesService,
    private router: Router
    ) { }

  ngOnInit() {
    // for getting the id of the book
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
    // this.getAllReviews();
  }

  // on submit
  onReviewSubmit() {
    // var id = this.sharedService.getData();
    console.log('on review submit id -', this.id);
    console.log('review submited data from form-', this.review);

    const review = {
      review: this.review
    };

    if (this.review.search(this.abusiveWords) > -1) {

      let comment = this.review;
      // replace abusive words and alert them
      comment = comment.replace(this.abusiveWords, '****');
      if (confirm(`Are you sure you want to submit + ' ' + ${comment} +' '+ !!`)) {
        this.dataService.addReview(this.id, comment).subscribe(data => {
          this.flashMessage.show('Thanks for review !!', {cssClass: 'alert-success', timeout: 3000});
          this.router.navigate(['/books']);
          console.log('data after review submit from subscribe-', data);
        });
      }
    }

    this.dataService.addReview(this.id, review).subscribe(data => {
      this.flashMessage.show('Thanks for review !!', {cssClass: 'alert-success', timeout: 3000});
      this.router.navigate(['/books']);
      console.log('data after review submit from subscribe-', data);
    });

  }

  // // get all reviews
  // getAllReviews(){
  //   this.dataService.getAllReviews(this.id).subscribe((res)=>{
  //     console.log('response of reviews -',res);
  //     this.reviews = res;
  //   });
  // }
  // event on file selct
  // onUploadFileClick(){
  //   console.log('uploadFileClick called !!');
  // }

  onSelectFile(event) {
    console.log('select file called !!-', event);
    this.selectedFile = event.target.files[0];
    console.log('selected file ', this.selectedFile);
  }

}
