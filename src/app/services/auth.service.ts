import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { responseFromApi, users } from '../interface';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Object;
  authToken: any;
  url = '/users';
  role: any;
  id: any;

  constructor(private http: HttpClient) { }

  // function to register the user
  getRegister(user: any) {
    const url = this.url + '/register';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<responseFromApi>(url, user, { headers : headers});
  }
  // function to get authenticated
  getAuthenticated(user) {
    const url = this.url + '/authenticate';
    const headers = new HttpHeaders();
    headers.append('Content-Tpe', 'applicaltion/json');
    return this.http.post<responseFromApi>(url, user, {headers: headers});
  }

  // function to get users
  getUsers() {
    const url = this.url + '/profile';
    this.loadToken();
    const headers = new HttpHeaders();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get<responseFromApi>(url, {headers: headers});
  }

  // function to get user from local storage
  getUsersLocalStorage() {
    this.user = JSON.parse(localStorage.getItem('user'));
    // this.id = this.user.id;
    // console.log()
    return this.user;
  }

  // function to store the data
  storeUserData(token, user) {
    // saving data into local storage
    localStorage.setItem('id_token', token);
    // localstorage can only store strings so convert object into strings
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
    this.id = user.id;
  }

  // methods to update password
  forgotPassword(email) {
    const url = '/users/forgot';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<responseFromApi>(url, email, {headers: headers});
  }

  // method for updating password
  updatePassword(username) {
    const url = '/users/update-password';
    const headers = new HttpHeaders();
    headers.append('Content-Tpe', 'application/json');
    return this.http.post<responseFromApi>(url, username, {headers: headers});
  }

  // function to logout
  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
    return false;
  }
  // function to check logged in
  loggedIn() {
    return tokenNotExpired('id_token');
  }

  // function to load token from local storage
  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
    const user = localStorage.getItem('user');
    this.user = JSON.parse(user);
  }
  // function to get admin
  getAdmin() {
    const url = this.url + '/admin';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get<responseFromApi>(url, {headers: headers});
  }

  // function to remove admin
  removeAdmin(id) {
    const url = this.url + '/removeadmin/' + id;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.delete<responseFromApi>(url, {headers: headers});
  }

  // function to add address
  addAddress(address) {
    const user = localStorage.getItem('user');
    // since the data obtained from localstorage is string so converting to json
    const User = JSON.parse(user);
    console.log('User-', User);
    const id = User.id;
    const url = this.url + `/addAddress/${id}`;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<responseFromApi>(url, address, {headers: headers});
  }

  // // function to get user's profile
  // getUserProfile(){
  //   const url = this.url + '/profile';
  //   const headers = new HttpHeaders();
  //   headers.append('Content-type','application/json');
  //   return this.http.get<responseFromApi>(url,{headers: headers});
  // }
  // // function to get random profile iamge
  // getProfileImage(){
  //   this.http.get
  // }
}
