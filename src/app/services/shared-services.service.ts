import { Injectable } from '@angular/core';
import { DataServiceService } from '../data-service.service';
import { AuthService } from '../services/auth.service';
import { users } from '../interface';
import { HttpHeaders } from '@angular/common/http';
import { Book } from '../book';
// import sgMail = require('@sendgrid/mail');
// var require : any;

@Injectable({
  providedIn: 'root'
})
export class SharedServicesService {

  constructor(private dataService: DataServiceService, private authService: AuthService) { }
  books: any;
  id: any;
  user: any;
  role: any;
  cart: any = [];
  bookID: any;

  // function to save data  from other component like navbar component.ts
  saveData(data) {
    // for saving the data for book
    this.books = data;
    // for saving the book id if book id is passed
    this.id = data;
    // for saving the role
    this.role = data;
  }


  // function to save the data to the cart
  saveToCart(data, bookID) {
    this.cart.push(data);
    this.bookID = bookID;
    // this.cart = data;
  }

  // // remove from cart
  // removeFromCart(){
  //   this.cart.splice()
  // }

  // function to send data from this component to other component
  getData() {
    return this.dataService.searchBook(this.books);
  }

  // function to get cart data
  getCartData() {
    return ({cart: this.cart, bookID: this.bookID});
  }

  // function to send mail
  // sendgridMail(){
  //   console.log('send grid');
  //   // const sgMail = require('@sendgrid/mail');
  //   sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  //     const msg = {
  //       to: this.getUserEmail(),
  //       from: 'rupeshiya@gmail.com',
  //       subject: 'Sending with SendGrid is Fun',
  //       text: 'and easy to do anywhere, even with Node.js',
  //       html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  //     };
  //     sgMail.send(msg);
  //     console.log('send grid mail sent');
  // }
  // for sending mail
  getUserEmail() {
    this.user = this.authService.getUsersLocalStorage();
    console.log('get email called -', this.user);
    return JSON.stringify(this.user.email);
  }

  // for checking admin presence
  adminFound() {
    return this.authService.getAdmin().subscribe(data => {
      if (data.success) {
        console.log('data of admin ', data);
        if (data.admin.length > 0) {
          console.log('data from admin found -', data);
          return true;
        }
       } else {
          console.log('admin not found');
          return false;
        }
    });
  }
}
