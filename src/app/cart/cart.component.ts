import { Component, OnInit } from '@angular/core';
import { SharedServicesService } from '../services/shared-services.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DataServiceService } from '../data-service.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
cartData: any;
bookID: any;
totalCost: any;

  constructor(
    private sharedService: SharedServicesService,
    private falshMessage: FlashMessagesService,
    private data: DataServiceService,
    private http: HttpClient,
    private router: Router
    ) { }

  ngOnInit() {
    this.getCartData();
    this.totalCosts();
  }

  // function to get the cart data
  getCartData() {
    this.cartData = this.sharedService.getCartData().cart;
  }

  // function to add address
  onBuyClick(id) {
    // this.sharedService.saveData(bookID);
    this.router.navigate([`/addAddress/${id}`]);
  }

  // function to calculate total costs
  totalCosts() {
    this.cartData.forEach(element => {
      this.totalCost = element.price;
      console.log('total price is -', this.totalCost);
    });
  }
}
