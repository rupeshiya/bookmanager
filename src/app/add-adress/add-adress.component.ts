import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DataServiceService } from '../data-service.service';
import { SharedServicesService } from '../services/shared-services.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-adress',
  templateUrl: './add-adress.component.html',
  styleUrls: ['./add-adress.component.css']
})
export class AddAdressComponent implements OnInit {
mobNumber: Number;
flatNumber: any;
city: any;
state: any;
nearby: any;
country: any;
user: any;
bookID: any;
book: any;
bookTitle: any;
pinCode: any;

  constructor(
    private authService: AuthService,
    private dataService: DataServiceService,
    private sharedService: SharedServicesService,
    private flashMessage: FlashMessagesService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bookID = params.id;
    });
  }

  // get userId from local storage
  getUserId() {
    this.user = this.authService.getUsersLocalStorage();
  }

  // bookID
  getBookId() {
    this.bookID = this.sharedService.getCartData().bookID;
  }

  // conforming the order
  onConformClick() {
    const address = {
      mobNumber: this.mobNumber,
      flatNumber: this.flatNumber,
      city: this.city,
      state: this.state,
      country: this.country,
      nearby: this.nearby,
      pinCode: this.pinCode
    };
    if (this.validateForm()) {
      console.log('address onConformClick ', address);
      if (confirm('Are you sure , You want to place order !!')) {
        this.authService.addAddress(address).subscribe((data) => {
          if (data.success) {
            console.log('Sending the address to the database');
            this.flashMessage.show('Successfully added address !!', {cssClass: 'alert-success', timeout: 3000});
            this.onPlaceOrder();
          } else {
            this.flashMessage.show('Unable to add address !!', {cssClass: 'alert-danger', timeout: 3000});
          }
        });
      }
    }
  }

  ////// error is bookID is undefined
  onPlaceOrder() {
    // const bookName = JSON.parse(book);
    this.getBookById();
    const bookName = {
      bookName: this.bookTitle
    };
    this.dataService.buyBook(bookName).subscribe((res) => {
      if (res.success) {
        console.log('Payment Done!!');
        this.flashMessage.show('Payment Done!!', {cssClass: 'alert-success', timeout: 3000});
      } else {
        console.log('Unable to make payment !!');
        this.flashMessage.show('Unable to make payment !!', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }

  // // function to get book by id
  getBookById() {
    this.dataService.getBook(this.bookID).subscribe((book) => {
      this.bookTitle = book.title;
    });
  }

  // function for validation
   validateForm(): boolean {
    if ((this.nearby === undefined || this.mobNumber === undefined || this.flatNumber === undefined  || this.city === undefined || this.state === undefined || this.country === undefined || this.pinCode === undefined) ) {

      this.flashMessage.show('Please fill all the info !!', {cssClass: 'alert-danger', timeout: 3000});
      return false;

    } else if (this.mobNumber.toString().length !== 10) {

      this.flashMessage.show('Mobile number is worng !!', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }
    return true;
  }

}
