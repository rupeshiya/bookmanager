import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
userProfile: any;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.getUser();
  }

  // function to get user's info
  getUser() {
    this.userProfile = this.authService.getUsersLocalStorage();
    console.log('user from dashboard -', this.userProfile);
  }
}
