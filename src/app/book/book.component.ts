import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { DataServiceService } from '../data-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SharedServicesService } from '../services/shared-services.service';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
pageLength = 10;
pageSize =  5;
pageSizeOptions = [1, 2 , 5 , 10];
currentPage = 1;
books: any;
admin: boolean;
likeCount: any;
dislikeCount: any;
like: Boolean;
dislike: Boolean;
loggedin: Boolean;

  constructor(
    private data: DataServiceService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private flashMessage: FlashMessagesService,
    private sharedService: SharedServicesService
    ) { }

  ngOnInit() {
    this.getBooks();
    this.adminFound();
  }

  // function to be executed on delete button
  onDelete(id: any) {
    this.data.deleteBook(id)
      .subscribe((data) => {
        this.getBooks();
      });

  }

  // function to get books from backend
  getBooks() {
    this.data.getBooks(this.pageSize, this.currentPage)
    .subscribe((books => {
      console.log(books);
      this.books = books.books;
      this.pageLength = books.totalBooks;
    }));
  }

  // function to navigate after edit on update button click
  updateBook(id: any) {
    this.router.navigate([`/book-edit/${id}`]);
  }

  // function to navigate to /book-details view
  getBookDetails(id: any) {
    this.router.navigate([`/book-details/${id}`]);
  }

  // admin component
  adminFound() {
    this.authService.getAdmin().subscribe(data => {
      if (data.success) {
        if (data.admin.length > 0) {
          this.flashMessage.show('Admin found !!', {cssClass: 'alert-success', timeout: 3000});
          this.admin =  true;
          return this.admin;
        }
       } else {
        window.location.reload();
          this.admin = false;
          this.flashMessage.show('Admin not found !!', {cssClass: 'alert-success', timeout: 3000});
          return this.admin;
        }
    });
  }

  // function to check conformation of delete
  confirmDelete(id: any, name: string) {
    if (confirm('Are you sure to delete ' + name)) {
      this.onDelete(id);
      this.flashMessage.show('Successfully deleted ' + name, {cssClass: 'alert-success', timeout: 2000});
      console.log('Deleting ' + name);
    } else {
      console.log('Delete cancelled');
    }
  }

  // function to add review
  onReviewClick(id) {
    this.router.navigate([`/review/${id}`]);
  }

  // function to add like and dislike functionalities
  onThumbsUpClick(id) {
    this.likesDislikesGuard();
    this.data.like(id).subscribe((res) => {
      if (res.success) {
        console.log('liked the book !!');
        this.flashMessage.show('Thanks for liking the book !!', {cssClass: 'alert-success', timeout: 2000 });
        this.likeCount = res.totalLikes;
      } else {
        console.log('something went wrong in liking the book !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-success', timeout: 2000});
      }
    });
  }

  onThumbsDownClick(id) {
    this.likesDislikesGuard();
    this.data.dislike(id).subscribe((res) => {
      if (res.success) {
        console.log('Disliked the book !!', {cssClass: 'alert-success', timeout: 2000});
        this.dislikeCount = res.totalDislikes;
        this.flashMessage.show('Disliked the book !!', {cssClass: 'alert-success', timeout: 2000});
      } else {
        console.log('Something went wrong !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-success', timeout: 2000});
      }
    });
  }

  // on book request
  onRequestClick() {
    this.router.navigate(['/book-request']);
  }

  // guide for likes and dislikes
  likesDislikesGuard(): Boolean {
    if (this.authService.loggedIn()) {
      this.loggedin = true;
      return true;
    } else {
      this.loggedin = false;
      this.router.navigate(['/login']);
      return false;
    }
  }

  // function for paginations
  onChangedPage(pageData: PageEvent) {
    // adding 1 since index starts from 0
    const currentPage = pageData.pageIndex + 1;
    console.log('current page -', currentPage);
    const pageSize = pageData.pageSize;
    console.log('cuurent pageSize - ', pageSize);
    console.log('pagination event - ', pageData);
    // fetching after changing pagination
    this.data.getBooks(pageSize, currentPage).subscribe(data => {
      this.books = data.books;
      this.pageLength = data.totalBooks;
    });
  }
}
