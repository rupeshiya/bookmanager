import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { DataServiceService } from '../data-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-book-request',
  templateUrl: './book-request.component.html',
  styleUrls: ['./book-request.component.css']
})
export class BookRequestComponent implements OnInit {
email: any;
BookRequest: any;
AuthorRequestedName: any;

  constructor(
    private router: Router,
    private dataService: DataServiceService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onSubmitClick() {
    const bookRequestedInfo = {
      RequesterEmail: this.email,
      BookName: this.BookRequest,
      AuthorName: this.AuthorRequestedName
    };
    this.dataService.bookRequested(bookRequestedInfo).subscribe((res) => {
      if (res.success) {
        console.log('requested the book !!');
        this.router.navigate(['/books']);
        this.flashMessage.show('Request sent !!', {cssClass: 'alert-success', timeout: 3000});
      } else {
        console.log('error in requesting book !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }
}
