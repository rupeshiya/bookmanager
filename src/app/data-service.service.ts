import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { responseFromApi } from './interface';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = '/api';
@Injectable({
  providedIn: 'root',
})
export class DataServiceService {

  books: any = [];
  book: any = [];
  isbn: any;
  author: any;
  title: any;
  publisher: any;
  constructor(private http: HttpClient) { }

  // function for error handling
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  // function for getting all the books
  // getBooks(): Observable<any> {
  //   return this.http.get(apiUrl).pipe(
  //     map(books =>{return books}),
  //     catchError(this.handleError)
  //     );
  // }

  getBooks(bookPerPage: number, currentPage: number) {
    const queryParameters = `?pageSize=${bookPerPage}&page=${currentPage}`;
    return this.http.get<responseFromApi>(apiUrl + queryParameters);
  }

  // function to get single books
  getBook(id: string): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.get(url, httpOptions);
  }

  postBook(data): Observable<any> {
    return this.http.post(apiUrl, data, httpOptions);
  }

  updateBook(id, data): Observable<any> {
    const Url = '/api/' + id;
    console.log('apiUrl', Url);
    return this.http.put(Url, data, httpOptions);
  }

  deleteBook(id: string): Observable<{}> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete(url, httpOptions);
  }

  // function to search book
  searchBook(book) {
    const url = '/api/search/' + book;
    return this.http.get(url, httpOptions);
  }

  // function to buy book
  buyBook(bookName) {
    return this.http.post<responseFromApi>('/paypal/pay', bookName, httpOptions);
  }

  // function to add review
  addReview(id, review) {
    const url = `/api/review/${id}`;
    return this.http.post<responseFromApi>(url, review, httpOptions);
  }

  // function to get all reviews
  getAllReviews(id) {
    const url = `/api/reviews/${id}`;
    return this.http.get<responseFromApi>(url, httpOptions);
  }

  // elastic search
  searchBookElastic(book) {
    const url = '/api/search/' + book;
    return this.http.get(url, httpOptions);
  }

  // book request method
  bookRequested(bookRequestedInfo) {
    const url = '/api/request-book';
    return this.http.post<responseFromApi>(url, bookRequestedInfo, httpOptions);
  }

  // add likes
  like(id) {
    const url = `/api/like/${id}`;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.put<responseFromApi>(url, {headers: headers});
  }

  // add dislike
  dislike(id) {
    const url = `/api/dislike/${id}`;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.put<responseFromApi>(url, {headers: headers});
  }
}
