import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']
})

export class BookCreateComponent implements OnInit {
  book: any = [];

    constructor(
      private data: DataServiceService,
      private router: Router,
      private flashMessage: FlashMessagesService
      ) { }

  ngOnInit() {

  }
  onSubmit(form: NgForm) {
    console.log('form', form.value);
    this.book =  form.value;
    const obj = {
      isbn: this.book.isbn,
      title: this.book.title,
      author: this.book.author,
      publisher: this.book.publisher,
      price: this.book.price
    };
    if (this.validateForm()) {
      this.data.postBook(obj)
      .subscribe((res) => {
        //  console.log('Done',res)
         this.router.navigate([`/books`]);
      });
    }
  }

  onSubmitButton() {
    console.log('submit button clicked');
    // this.data.postBook(this.book);
  }
    // function for validation
    validateForm(): boolean {
      if ((this.book.isbn === undefined || this.book.title === undefined || this.book.author === undefined  || this.book.publisher === undefined || this.book.price === undefined ) ) {

        this.flashMessage.show('Please fill all the info !!', {cssClass: 'alert-danger', timeout: 3000});
        return false;
      }
      return true;
    }
}
