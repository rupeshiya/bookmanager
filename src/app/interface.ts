export interface users {
  _id: string;
  name: string;
  username: string;
  password: string;
  email: string;
  role?: string;
}
export interface responseFromApi {
  success: boolean;
  msg: string;
  token?: string;
  user?: users;
  role?: string;
  admin?: any;
  totalBooks?: any;
  books?: any;
  reviews?: any;
  totalLikes?: any;
  totalDislikes?: any;
}
