import { Component, OnInit } from '@angular/core';
import { SharedServicesService } from '../services/shared-services.service';
import { DataServiceService } from '../data-service.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-search-data',
  templateUrl: './search-data.component.html',
  styleUrls: ['./search-data.component.css']
})
export class SearchDataComponent implements OnInit {
books: any;
admin: boolean;
pageSize = 1;
currentPage = 1;
pageLength = 1;
pageSizeOptions = [1 , 2 , 5 , 10];

  constructor(
    private sharedService: SharedServicesService,
    private dataService: DataServiceService,
    private router: Router,
    private authService: AuthService,
    private flashMessage: FlashMessagesService
    ) { }

  ngOnInit() {
    // for getting admin
    this.adminFound();
    // for getting the search results
    this.sharedService.getData().subscribe(data => {
      console.log('books data from search component -', data);
        this.books = data;
      if (this.books.length === 0) {
        this.flashMessage.show('No book found for your search !!', {cssClass: 'alert-success', timeout: 3000});
        console.log('No book found for this search');
      }
    });
  }


  // function to be executed on delete button
  onDelete(id: any) {
    // var books = this.books;
    this.dataService.deleteBook(id)
      .subscribe(() => {
        this.getBooks();
      });

  }

  // function to get books from backend
  getBooks() {
    this.dataService.getBooks(this.pageSize, this.currentPage)
    .subscribe((books => {
      console.log(books);
      this.books = books.books;
      this.pageLength = books.totalBooks;
      if (this.books.length === 0) {
        this.flashMessage.show('Books not found !!', {cssClass: 'alert-success', timeout: 2000});
      }
    }));
  }
  // function to navigate after edit on update button click
  updateBook(id: any) {
    this.router.navigate([`/book-edit/${id}`]);
  }
  // function to navigate to /book-details view
  getBookDetails(id: any) {
    this.router.navigate([`/book-details/${id}`]);
  }

  // function to check admin presence
  adminFound() {
    this.authService.getAdmin().subscribe(data => {
      if (data.admin.length > 0) {
        this.admin = true;
        return this.admin;
      } else {
        this.admin = false;
        return this.admin;
      }
    });
  }
  // function to check conformation of delete
  confirmDelete(id: any, name: string) {
    if (confirm('Are you sure to delete ' + name)) {
      this.onDelete(id);
      this.flashMessage.show('Successfully deleted ' + name, {cssClass: 'alert-success', timeout: 2000});
      console.log('Deleting ' + name);
    } else {
      console.log('Delete cancelled');
    }
  }

  // function to add review
  onReviewClick(id) {
    // this.sharedService.saveData(id);
    this.router.navigate([`/review/${id}`]);
  }
  // // function for paginations
  // onChangedPage(pageData: PageEvent){
  //   // adding 1 since index starts from 0
  //   this.currentPage = pageData.pageIndex + 1;
  //   this.pageSize = pageData.pageSize;
  //   console.log('pagination event - ',pageData);
  //   // fetching after changing pagination
  //   this.dataService.getBooks(this.pageSize,this.currentPage).subscribe(data=>{
  //     this.books =  data.books;
  //     this.pageLength = data.totalBooks;
  //   })
  // }
}
