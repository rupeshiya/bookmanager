const expect = require('chai').expect;
const assert = require('assert');
const BookSchema = require('../models/book');

describe('Book Schema',()=>{

    it('Should be invalid if isbn is empty',(done)=>{
        var b = new BookSchema();
        b.validate((err)=>{
            expect(err.errors.isbn).to.exist;
            done();
        });
    });

    it('Should be invalid if isbn is empty',(done)=>{
        var b = new BookSchema();
        b.validate((err)=>{
            expect(err.errors.title).to.exist;
            done();
        });
    });

    it('Should be invalid if isbn is empty',(done)=>{
        var b = new BookSchema();
        b.validate((err)=>{
            expect(err.errors.author).to.exist;
            done();
        });
    });

    it('Should be invalid if isbn is empty',(done)=>{
        var b = new BookSchema();
        b.validate((err)=>{
            expect(err.errors.description).to.exist;
            done();
        });
    });

    it('Should be invalid if isbn is empty',(done)=>{
        var b = new BookSchema();
        b.validate((err)=>{
            expect(err.errors.price).to.exist;
            done();
        });
    });
  
});