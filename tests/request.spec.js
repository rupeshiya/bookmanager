const expect = require('chai').expect;
const assert = require('assert');

const RequestModel = require('../models/request');

describe('BookRequestModel', ()=>{
  
    it('Should be invalid if requesterEmail is undefined',(done)=>{
        var BookRequestModel = new RequestModel();
        BookRequestModel.validate((err)=>{
            expect(err.errors.requesterEmail).to.exist;
            done();
        });
    });

    it('Should be invalid if bookName is empty',(done)=>{
        var BookRequestModel = new RequestModel();
        BookRequestModel.validate((err)=>{
            expect(err.errors.bookName).to.exist;
            done();      
        });
    });

    it('Should be invalid if authorName is empty ',(done)=>{
        var BookRequestModel = new RequestModel();
        BookRequestModel.validate((err)=>{
            expect(err.errors.authorName).to.exist;
            done();
        });
    });
  
});