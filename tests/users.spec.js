const expect = require('chai').expect;
const assert = require('assert');
const UsersModel = require('../models/users');

describe('User Model',()=>{

    it('Should be invalid if username is empty ',(done)=>{
        var users = new UsersModel();
        users.validate((err)=>{
            expect(err.errors.username).to.exist;
            done();
        });
    });

    it('Should be invalid if email is undefined',(done)=>{
        var users = new UsersModel();
        users.validate((err)=>{
            expect(err.errors.email).to.exist;
            done();
        });
    });

    it('Should be invalid if name is empty',(done)=>{
        var users = new UsersModel();
        users.validate((err)=>{
            expect(err.errors.name).to.exist;
            done();
        });
    });

    it('Should be invalid if role is empty',(done)=>{
        var users = new UsersModel();
        users.validate((err)=>{
            expect(err.errors.role).to.exist;
            done();
        });
    });

    it('Should be invalid if password is empty',(done)=>{
        var users = new UsersModel();
        users.validate((err)=>{
            expect(err.errors.password).to.exist;
            done();
        });
    });

});

