const express = require('express');
const paypal = require('paypal-rest-sdk');
const router = express.Router();
const cors = require('cors');
require('dotenv').config();
const {mailOptions , sendMailOnRegister} = require('./mail');

//configure paypal payment method
paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': process.env.clientID,
    'client_secret': process.env.clientSecret
});

//handeling post request /paypal/pay
router.post('/pay',(req,res)=>{
    mailOptions.to = req.user.email;
    var create_payment_json = {
        'intent': 'sale',
        'payer': {
            'payment_method': 'paypal'
        },
        'redirect_urls': {
            'return_url': 'http://localhost:4000/paypal/success',
            'cancel_url': 'http://localhost:4000/paypal/cancel'
        },
        'transactions': [{
            'item_list': {
                'items': [{
                    'name': 'some_name',
                    'sku': 'item',
                    'price': '1.00',
                    'currency': 'USD',
                    'quantity': 1
                }]
            },
            'amount': {
                'currency': 'USD',
                'total': '1.00'
            },
            'description': 'This is the payment description.'
        }]
    };
    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            res.status(500).json({success: false,msg:'error -'+ error});
            console.log(error);
        }
        else {
            for(var i = 0; i < payment.links.length; i++){
                if(payment.links[i].rel === 'approval_url' && payment.links[i].method === 'REDIRECT'){
                    console.log('payment is processing');
                    sendMailOnRegister();
                    res.redirect(payment.links[i].href);
                }
            }
        }
    });
});
//on getting Success /paypal/success
router.get('/success',(req,res)=>{
    const payerId = req.query.payerId;
    const paymentId = req.query.paymentId;
    const execute_payment_json = {
        'payer_id': payerId,
        'transactions': [{
            'amount': {
                'currency': 'USD',
                'total': '25.00'
            }
        }]
    };
    //after getting success executing payment
    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
        } else {
            console.log(JSON.stringify(payment));
        }
    });

});

// for /paypal/cancel
router.get('/cancel',(req,res)=>{
    console.log('Cancelled transcations !!');
    res.status(500).json({success: false,msg:'unable to make payment'});
});

module.exports = router;
