const mongoose = require('mongoose');
const express = require('express');
const async = require('async');
const nodemailer = require('nodemailer');
require('dotenv').config();

const transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth:{
        user: `${process.env.user}`,
        pass: `${process.env.pass}`
    }
});

const mailOptions = {
    to: null,
    from:'devilwebdev@gmail.com',
    subject:'testing mail sent !!',
    text: 'hello world !!'
};

const sendMailOnRegister = function(){
    transporter.sendMail(mailOptions,(err)=>{
        if(err){
            console.log('error in sending the mail !!',err);
        }
        console.log('Successfully sent the mail !!');
    });
};

module.exports = {
    transporter,
    mailOptions,
    sendMailOnRegister
};