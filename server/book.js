const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const { sendMailOnRegister, mailOptions } = require('./mail');
const async = require('async');
// loading models
const Book = require('../models/book');
const Review = require('../models/reviews');
require('dotenv').config();

// mapping between elastcsearch and mongodb
// Book.createMapping((err,mapping)=>{
//     if(err){
//         console.log('error in mapping the mongodb to elasticSearch',err);
//     } else {
//         console.log('mapping created between mongo and elastcsearch');
//         console.log('mapping -',mapping);
//     }
// });

// copying the mongodb doc to elasticsearch
// const stream = Book.synchronize();
// var count = 0;

// // event for elasticsearch
// stream.on('data',()=>{
//     count++;
// });
// stream.on('close',()=>{
//     console.log('closed the elastcisearch with total !! -'+count);
// });
// stream.on('error',(err)=>{
//     console.log('error in events ',err);
// });



// connect to mongoose
mongoose.connect(process.env.mongoURI,{
    useNewUrlParser: true
})
    .then(()=>{
        console.log('mongo connected');
    })
    .catch((err)=>{
        console.log(err);
    });

// routes /book
router.get('/',(req,res)=>{
    // res.send('Welcome to the book');
    // here + is used to convert string into number
    const pageSize = +req.query.pageSize;
    const currentPage =  +req.query.page;
    var bookFetched = undefined;
    const query =  Book.find({}).lean();
    // checking for pageSize and currentpage
    if(pageSize && currentPage){
        query
            .skip((pageSize *(currentPage - 1)))
            .limit((pageSize));
    }
    query
        .then((books)=>{
            console.log(books);
            bookFetched = books;
            return Book.estimatedDocumentCount();
        })
        .then((count)=>{
            res.status(200).json({
                books: bookFetched,
                totalBooks : count
            });
        })
        .catch((err)=>{
            console.log(err);
            res.status(500).json(err);
        });
});

// routes for rendering one book
router.get('/:id',(req,res)=>{
    Book.findById(req.params.id)
        .lean()
        .then((book)=>{
            res.status(200).json(book);
        })
        .catch((err)=>{
            res.status(500).json(err);
        });
});

// To post book -->/api 
router.post('/',(req,res)=>{
    const book = {
        isbn: req.body.isbn,
        title: req.body.title,
        author: req.body.author,
        description: req.body.description,
        published_year: req.body.published_year,
        publisher: req.body.publisher,
        price: req.body.price
    };
    console.log('req.body',req.body);
    console.log('book-add',book);
    new Book(book)
        .save()
        .then((books)=>{
            console.log('successfully saved book');
            // res.redirect('/');
            res.status(200).json(books);
        })
        .catch((err)=>{
            console.log(err);
            res.status(500).json(err);
        });

});

// 

// routes for /books edit
router.put('/:id',(req,res)=>{
    Book.findById(req.params.id)
        .then((book)=>{
            book.isbn = req.body.isbn;
            book.title = req.body.title;
            book.author = req.body.author;
            book.publisher = req.body.publisher;
            book.description = req.body.description;
            book.published_year = req.body.published_year;
            book.save()
                .then((book)=>{
                    console.log('book updated');
                    res.status(200).json(book);
                });
        })
        .catch((err)=>{
            console.log(err);
        });
});

// for deleting the book
router.delete('/:id',(req,res)=>{
    Book.findByIdAndRemove(req.params.id)
        .then((book)=>{
            console.log('Book deleted');
            res.status(200).json(book);
            // res.send(`<h3>Book successfully deleted !!</h3>`);
            // res.redirect('/');
        })
        .catch((err)=>{
            console.log(err);
            // res.send(`<h3>Error in server !!</h3>`);
            res.status(500).json({msg: 'error in server!!'});
        });
});

// for searching books
router.get('/search/:book',(req,res)=>{
    // here options is used of case-insesitive
    Book.find({title:  {'$regex': req.params.book, '$options': 'i'}})
        .lean()
        .then((books)=>{
            res.status(200).json(books);
        })
        .catch(()=>{
            res.status(500).json({msg: 'internal error in books.js'});
        });
});

// // implementing elastic search for book searching
// router.post('/search/:book',(req,res)=>{
//     res.redirect('/search?query='+req.body.searchText);
// });
// router.get('/search',(req,res)=>{
//     if(req.query.query){
//         Book.search({
//             query_string:{ query: req.query.query }
//         },(err,result)=>{
//             if(err){
//                 console.log('error in elasticsearch ',err);
//                 res.status(500).json({success: false,msg:'unable to search'});
//             } else {
//                 console.log('result from elastic serach -',result);
//                 res.status(200).json(result);
//             }   
//         });
//     }
// });
const multer = require('multer');
var upload = multer({ dest: 'uploads/' });
// for adding review
router.post('/review/:id',(req,res)=>{
    Book.findById(req.params.id)
        .then((book)=>{
            const review = escape(req.body.review);
            book.review.push(review);
            // book.review = req.body.review;
            // book.data.image = req.files[0];
            book.save()
                .then((book)=>{
                    console.log('mssg from review api - success');
                    res.status(200).json(book);
                })
                .catch((err)=>{
                    console.log('unable to save the review due to -',err);
                    res.status(500).json(err);
                });
            
        })
        .catch((err)=>{
            console.log('mssg from review api- err');
            res.status(500).json(err);
        });
});

// // function to post reviews
// router.post('/review/:id',(req,res)=>{
//     Book.findById(req.params.id)
//         .populate('reviews')
//         .then((book)=>{
//             console.log('book from review-',book);
//             console.log('book from review-',book.review);
//             console.log('book.reviews from review-',book.reviews);
//             book.reviews.book = book._id;
//             book.reviews.review.push(req.body.review);
//             book.save()
//                 .then((book)=>{
//                     console.log('mssg from review api - success');
//                     res.status(200).json(book);
//                 })
//                 .catch((err)=>{
//                     console.log('unable to save the review due to -',err);
//                     res.status(500).json(err);
//                 });
//         })
//         .catch((err)=>{
//             res.status(500).json({success: false, msg: 'unable to save review !!',err: err});
//         });
// });



// // function for getting all reviews
// router.get('/reviews/:id',(req,res)=>{
//     Review.find({book: req.params.id})
//         .populate('book')
//         .then((data)=>{
//             console.log('data from review - ',data);
//             const bookName = data.book.title;
//             const reviews = data.review[0];
//             res.status(200).json({success: true,msg:'Got the review and book info !!',reviews: reviews, book: bookName});
//         })
//         .catch((err)=>{
//             res.status(500).json({success: false, msg: 'error occured in fetching reviews !!',err: err});
//         });
// });

// add routes to like and dislike i.e /api/like/:id
router.put('/like/:id',(req,res)=>{
    Book.findByIdAndUpdate({_id: req.params.id },{$inc: {upVote: 1}},{new: true}, (err, book)=>{
        if(err){
            console.log('error in liking server side !!');
            res.status(500).json({success: false, msg: 'Error in server liking !!'});
        } 
        if(book) {
            console.log('after upvote - ', book.upVote);
            res.status(200).json({success: true, msg: 'Successfully liked !!', totalLikes: book.upVote });
        }
    });
});

// route /api/dislike/:id
router.put('/dislike/:id',(req,res)=>{
    Book.findByIdAndUpdate({_id: req.params.id},{$inc: {downVote: 1}},{new: true},(err, book)=>{
        if(err){
            console.log('error in the disliking in server !!');
            res.status(200).json({success: false, msg: 'Error in server disliking'}); 
        } 
        if(book){
            console.log('after downvote ', book.downVote);
            res.status(200).json({success: true, msg: 'Successfully disliked !!', totalDislikes: book.downVote });
        }
    });
});

// request for books routes /api/request-book
const BookRequest = require('../models/request');
router.post('/request-book',(req,res)=>{
    const request = {
        requesterEmail: req.body.RequesterEmail,
        bookName : req.body.BookName,
        authorName: req.body.AuthorName
    };
    new BookRequest(request).save()
        .then((book)=>{
            console.log('successfully saved the book request !!');
            mailOptions.to = req.body.RequesterEmail;
            mailOptions.subject = 'Book Request';
            mailOptions.text = 'Hello \n\n' + 'Thanks for your interest !!\n' +`Your request of the book ${req.body.BookName} is successfully sent , We will soon contact you if we can arrange that book for you.\n` + ' Happy reading !!';
            sendMailOnRegister();
            res.status(200).json({success: true, msg:'Successfully saved the book request !!',book:book});
        })
        .catch((err)=>{
            console.log('error in saving the request !!');
            res.status(500).json({success: false, msg:'Server error !!',err: err});
        });    
});

module.exports = router;


