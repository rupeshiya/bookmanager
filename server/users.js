const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const async = require('async');
const mongoosastic = require('mongoosastic');
const nodemailer = require('nodemailer');
const { mailOptions , sendMailOnRegister } = require('./mail');
const crypto = require('crypto');
require('dotenv').config();

// loading model
require('../models/users');
const User = require('../models/users');
var email ; // for storing email to sending email
require('../models/address');

// =================================== USER REGISTRATION ======================================= //
// ============================================================================================= //

// /users/register route
router.post('/register',(req,res)=>{
    async.waterfall([
        function(done){
            // check for existing user
            const username = req.body.username;
            User.getUserByUsername(username, (err,user)=>{
                if(err){
                    console.log('error in the server !!');
                    res.status(500).json({success: false, msg:'error in the server !!'});
                }
                if(user){
                    console.log('user alreday exists !!');
                    res.status(200).json({success: false, msg:'User already exists !!'});
                }
                if(!user){
                    email =  req.body.email;
                    const newUser = new User({
                        name: req.body.name,
                        username: req.body.username,
                        email: req.body.email,
                        password: req.body.password,
                        role: req.body.role
                    });
                    User.addUser(newUser,(err,user)=>{
                        if(err){
                            console.log('error in users.js',err);
                            res.status(500).json({success:false, msg: 'unable to register'});
                        } else {
                            res.status(200).json({success: true, msg: 'User registered'});
                            done(err,user);
                        }
                    });
                }
            });  
        },
        function(user,done){       
            mailOptions.to = user.email;
            mailOptions.subject = 'Registration !!';
            mailOptions.text = 'Thanks for registering yourself on BookManager !!\n\n' + 'We are happy to have you !!\n\n' + 'Happy shopping !!';
            sendMailOnRegister();
            // done(err);
        }
    ],(err)=>{
        console.log('error in async waterfall !!',err);
    });
});


// =================================== USER AUTHENTICATION ==============================//
// ====================================================================================== //

// /users/authenticate route
router.post('/authenticate',(req,res)=>{
    const username =  req.body.username;
    const password =  req.body.password;
    User.getUserByUsername(username,(err,user)=>{
        if(err){
            console.log('error in username ',err);
        }
        if(!user){
            return res.json({success: false,msg:'user does not exists'});
        } 
        // compare password for authentication
        // user.password is hashed password
        User.comparePassword(password, user.password,(err,isMatch)=>{
            if(err){
                console.log('error in users.js ',err);
            }
            if(isMatch){
                // making token
                const token = jwt.sign({data: user}, process.env.secret, {
                    expiresIn: 604800 // 1 week
                });
                // sending response to the front-end
                res.json({
                    success: true,
                    token: 'JWT '+token,
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        email: user.email,
                        role: user.role
                    }
                });
            } else {
                return res.json({success: false, msg: 'Wrong password'});
            }
        });
    });
});
// =================================== USER AUTHENTICATION ENDS =========================== //
// ======================================================================================== //

// /users/profile route
router.get('/profile',(req,res)=>{
    res.status(200).json({success: true,user: req.user});
});

// /users/validate route
router.get('/validate',(req,res)=>{
    res.send('validate works!!');
});

// =================================== GET ADMIN INFO  ==================================== //
// ======================================================================================== //

// routes for admin
router.get('/admin',(req,res)=>{
    User.find({role: 'admin'})
        .lean()
        .then((data) =>{
            res.json({success : true ,msg: 'admin found',admin:data});
        })
        .catch((err)=>{
            res.json({success: false , msg: 'admin not found'});
        });
});

// =================================== REMOVE ADMIN ======================================= //
// ======================================================================================== //

// routes to remove admin
router.delete('/removeadmin/:id',(req,res)=>{
    User.findByIdAndRemove(req.params.id)
        .then((data)=>{
            res.json({success: true, msg: 'removed admin',admin: data});
        })
        .catch((err)=>{
            res.json({success: false, msg: 'unable to delete or not found', admin: err});
        });
});

// =================================== ADD USER ADDRESS ================================== //
// ====================================================================================== //

const Address = require('../models/address');
// // function to post the address /users/addAdress
// router.post('/addAddress/:id',(req,res)=>{
//     User.findById(req.params.id)
//         .populate('address')
//         .exec((err,user)=>{
//             if(err){
//                 console.log('Error in saving the address in the database');
//                 res.status(500).json({success: false, msg:'unable to save the address!!'});
//             } else {
//                 console.log('users from server-',user.address);
//                 user.address.city = req.body.city;
//                 user.address.state = req.body.state;
//                 user.address.flatNumber = req.body.flatNumber;
//                 user.address.mobNumber =  req.body.mobNumber;
//                 user.address.country = req.body.country;
//                 user.address.nearby = req.body.nearby;
//                 // user.address.push(address);
//                 user.save()
//                     .then((data)=>{
//                         console.log('Address data saved successfully in the database');
//                     })
//                     .catch((err)=>{
//                         console.log('Error in saving the address in the databse');
//                     });
//                 res.status(200).json({success: true, msg:' Address successfully saved',address: user});
//             }
//         });
// });

// add address
router.post('/addAddress/:id',(req,res)=>{
    async.waterfall([
        function(callback){
            User.findById(req.params.id)
                .then((user)=> callback(null,user))
                .catch((err)=> {
                    console.log('error in saving address ',err);
                    res.status(500).json({success: false,msg:'unable to save!!'});
                });
        },
        function(user,callback){
            const newAddress =new Address();
            newAddress.city = req.body.city;
            newAddress.state = req.body.state;
            newAddress.country = req.body.country;
            newAddress.nearby = req.body.nearby;
            newAddress.flatNumber = req.body.flatNumber;
            newAddress.mobNumber = req.body.mobNumber;
            newAddress.pinCode = req.body.pinCode;
            newAddress.user = user._id;
            newAddress.save()
                .then((data)=>{ console.log('succesfully saved the address !!'); })
                .catch((err)=>{ 
                    console.log('unable to save !!');
                    res.status(500).json({success: false,msg:'unable to save!!'});
                });
        } 
    ]);
    res.status(200).json({success: true, msg:' Address successfully saved'});    
});

// ===================================POST FORGOT PASSWORD =============================== //
// ======================================================================================= //
router.post('/forgot',(req,res)=>{
    async.waterfall([
        function(done){
            crypto.randomBytes(20,(err,buf)=>{
                const token = buf.toString('hex');
                done(err, token);
                console.log('token created by crypto -', token);
            });
        },
        function(token,done){
            User.findOne({email: req.body.email})
                .then((user)=>{
                    user.resetPasswordToken = token;
                    user.resetPasswordTime = Date.now() + 360000; // 1hr
                    user.save((err)=>{
                        done(err,token,user);
                    });
                })
                .catch((err)=>{
                    console.log('error in the server  !!',err);
                });
        },
        function(token,user,done){
            mailOptions.to = user.email;
            mailOptions.subject = 'Resetting passsword !!';
            mailOptions.text = 'Please click on the link to reset the password !!\n\n' + 'link is -' + 'http://' + req.headers.host +'/users/reset/'+ token +'\n\n';
            // http://localhost:4000/users/reset/:token
            sendMailOnRegister();
            console.log('successfully sent the mail !!');
            res.status(200).json({success: true, msg:'successfully sent the mail !!'});
        }
    ],(err)=>{
        console.log('error in async code !!',err);
    });
});

// =================================== FORGOT PASSWORD GET=============================== //
// ====================================================================================== //

// ROUTE /users/reset/:token for rendering new password page
router.get('/reset/:token',(req,res)=>{
    User.findOneAndUpdate({resetPasswordToken: req.params.token, resetPasswordTime:{$gt: Date.now()}},(err,user)=>{
        if(err){
            console.log('error in finding the token !!');
            res.status(500).json({success: false, msg: 'unable to find the token or time expired!'});
        }
        if(!user){
            console.log('user not found !!');
            res.status(200).json({success: false, msg: 'User does\'nt exists !!'});
        }
        // res.render('index',{ user: user });
        res.status(200).json({success: true, msg:'Reset token found !!',user: user});
    });
});

// ================================== FOR UPDATING PASSWORD =============================== //
// ======================================================================================== //
// /users/update-password
router.post('/update-password',(req,res)=>{
    User.findOneAndUpdate({username: req.body.username})
        .then((user)=>{
            console.log('user -',user);
            user.resetPasswordToken = null;
            user.resetPasswordTime = null;
            user.password = req.body.password;
            user.save()
                .then(()=>{
                    console.log('saved password!!');
                    mailOptions.to = req.body.email;
                    mailOptions.subject = 'Password changed !!';
                    mailOptions.text = 'Successfully changed the password !!\n\n';
                    sendMailOnRegister();
                    res.status(200).json({success: true, msg:'saved password !!'});
                })
                .catch((err)=>{
                    console.log('error in saving password !!',err);
                });
        })
        .catch((err)=>{
            console.log('error in updating password !!',err);
            res.status(500).json({success: false, msg:'Error in server side in updating the password !!'});
        });
});

module.exports = router;